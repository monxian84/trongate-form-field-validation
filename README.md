This validation function can be used stand alone from the official trongate one.
Just add it to your module and call it on your form page.

To use just add unique characters of your error message as an array.
Trongate's errors are in the form of "The email" "The password" "The confirm"...
Even custom callbacks work. 

Watch out for duplicates(it's unavoidable) in custom error messages.
'The email field is required' and 'The email/password is incorrect' will pass with 'The email'.
Do something like 'Incorrect email/password' and use 'Incorrect email' as the search term 
in the calling array. 

The array terms for each field should be unqiue.

Members is the example module here
`<?php 
  $errors = Modules::run('members/validation_errors_ext',['The email', 'The password', 'The confirm', 'This email']);
 ?>`
The opening and closing html tags in the function params are optional.
The function returns an array of errors. So under your html input fields add.
To the specific input field. For example.

`<label for="password">Password</label>`
`<input type="password" name="password">`
`<?php if(isset($errors['The password']))echo $errors['The password']?>`
or 
`echo (isset($errors['The password'])) ? $errors['The password'] : '';`

Finally in your validation_helper->set_rules as an example

`$this->validation_helper->set_rules('email', 'email', 'required|min_length[7]|max_length[255]|valid_email_address|valid_email|callback_email_taken');`
The second parameter is the search term matches. 

This has been sparely tested.


